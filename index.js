const express = require('express')
const uuid = require('uuid')

const app = express()

app.use(express.json())

const baseMusicas = []

function cadastrarMusica(nome) {
    if (nome !== null && nome != undefined) {
        for (let index in baseMusicas) {
            if (baseMusicas[index].nome === nome)
                return "Musica já cadastrada!"
        }
    
        let musicaNova = {
            id: uuid.v4(),
            nome: nome,
            avaliacao: 0
        }
        baseMusicas.push(musicaNova)
        return musicaNova
    } else {
        return "Informe o nome da música"
    }
}

function listarMusicas() {
    baseMusicas.sort((a,b) => {
        return a.avaliacao > b.avaliacao ? -1 : a.avaliacao < b.avaliacao ? 1 : a.nome.localeCompare(b.nome)
    })
    return baseMusicas
}

function avaliarMusica(nome, avaliacao) {
    if (nome != null && nome != undefined) {
        avaliacao = Number(avaliacao)
        if (avaliacao <= 5 && avaliacao >= 1) {
            for (let index in baseMusicas) {
                if (baseMusicas[index].nome === nome) {
                    baseMusicas[index].avaliacao = avaliacao
                    return baseMusicas[index]
                }
            }
            return "Música não existe!"
        } else {
            return "A avaliação deve ser de 1 a 5"
        }
    } else {
        return "Informe o nome da música"
    }
}


app.post('/cadastrar', (req, res) => {
    let musicaCadatrada = cadastrarMusica(req.query.nome)
    res.status(200).send(musicaCadatrada)
})

app.get('/listar', (req, res) => {
    let musicas = listarMusicas()
    res.status(200).send(musicas)
})

app.post('/avaliar', (req, res) => {
    let musicaAvaliada = avaliarMusica(req.query.nome, req.query.avaliacao)
    res.status(200).send(musicaAvaliada)
})

app.get('/teste', (req, res) => {
    res.status(200).send("Funcionando!")
})

app.listen(4200, async () => {
    console.log("Avaliação de Musicas. Porta 4200")
})


