const assert = require('chai').assert;
const axios = require('axios');

const baseMusicas = require('../index.js').baseMusicas;

async function cadastrarMusica(nome) {
    const response = await axios.post(`http://localhost:4200/cadastrar?nome=${nome}`);
    return response.data;
}

async function avaliarMusica(nome, avaliacao) {
   const response = await axios.post(`http://localhost:4200/avaliar?nome=${nome}&avaliacao=${avaliacao}`);
   return response.data;
}

async function obterLista() {
    const response = await axios.get('http://localhost:4200/listar');
    return response.data;
}

describe('Teste de ordenação', function() {
   before(async function() {
       await cadastrarMusica('Música A');
       await cadastrarMusica('Música C');
       await cadastrarMusica('Música B');
       await avaliarMusica('Música A', 3);
       await avaliarMusica('Música B', 5);
       await avaliarMusica('Música C', 4);
   });

   it('Deve retornar a lista de músicas ordenada corretamente', async function() {
       const listaMusicas = await obterLista();
       const musicasOrdenadas = listaMusicas.sort((a, b) => {
           if (a.avaliacao !== b.avaliacao) {
               return b.avaliacao - a.avaliacao;
           } else {
               return a.nome.localeCompare(b.nome);
           }
       });

       assert.deepEqual(listaMusicas, musicasOrdenadas);
   });
});

describe('Teste de cadastro', function() {

   it('Deve cadastrar uma música com avaliação inicial igual a zero', async function() {
       const nomeMusica = 'NovaMúsica';
       const musicaCadastrada = await cadastrarMusica(nomeMusica);
       
       assert.equal(musicaCadastrada.nome, nomeMusica);
       assert.equal(musicaCadastrada.avaliacao, 0);
   });
});